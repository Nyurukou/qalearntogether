package tinkingInJava.chapter3;

/**
 * Created by Nyarukou SAMA on 14.01.2016.
 * Testing Exponential record.
 * Put on console the biggest and the smallest value of float and double in exponential form
 */
public class Task9 {
    /**
     * main - is a program point input (main thread)
     * @param args
     * args from command line
     */
    public static void main(String[] args) {
        expoRecord();
    }

    /**
     * This method shows min and max of float and double in computerized scientific notation.
     */
    public static void expoRecord(){
        float expFMin = Float.MIN_VALUE;
        float expFMax = Float.MAX_VALUE;
        double expDMin = Double.MIN_VALUE;
        double expDMax = Double.MAX_VALUE;
        System.out.printf("Минимум для типа float в expoRecord составляет \"%e\", максимум - \"%e\"%n", expFMin, expFMax);
        System.out.printf("Минимум для типа double в expoRecord составляет \"%e\", максимум - \"%e\"%n", expDMin, expDMax);
    }
}
