package tinkingInJava.chapter3;

/**
 * Created by Nyarukou SAMA on 14.01.2016.
 * Bitwise operator demonstration. Define two int variables as sequence of zeros and units.
 * First var should have zero in least significant bit (lsb).
 * Second var should have zero in most significant bit(msb).
 */
public class Task10 {
    public static void main(String[] args) {
        bitwiseDemo();
    }

    /**
     * This method shows the result of bitwise operations. Just try some more by yourself.
     */
    private static void bitwiseDemo() {
        int var1 = 0xAAAAAAAA;
        int var2 = 0x55555555;
        System.out.println(var1);
        System.out.println(Integer.toBinaryString(var1));
        System.out.println(var2);
        System.out.println(Integer.toBinaryString(var2));
        System.out.printf("Result of & %s%n", Integer.toBinaryString(var1 & var2));
        System.out.printf("Result of | %s%n", Integer.toBinaryString(var1 | var2));
        System.out.printf("Result of ^ %s%n%n", Integer.toBinaryString(var1 ^ var2));
        var2 = 0x7FFFFFFF;
        System.out.printf("              This var2 now %s%n", var2);
        System.out.printf("              This var2 now %s%n", Integer.toBinaryString(var2));
        System.out.printf("     Result of <<1 for var2 %s%n", Integer.toBinaryString(var2 <<= 1));
        System.out.printf("     Result of >>1 for var2 %s%n", Integer.toBinaryString(var2 >>= 1));
        System.out.printf("  And what we have after :D %s%n", var2);
        System.out.println("Magic!");
    }
}
