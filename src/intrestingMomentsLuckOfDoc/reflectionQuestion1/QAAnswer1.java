package intrestingMomentsLuckOfDoc.reflectionQuestion1;

public class QAAnswer1 {

    public static void main(String[] args) {
        printObjectType(new Cat());
        printObjectType(new Bird());
        printObjectType(new Lamp());
        printObjectType(new Cat());
        printObjectType(new Dog());
    }

    public static void printObjectType(Object o) {
        ((Printable) o).printClassName();
    }

    public static class Cat implements Printable {

        @Override
        public void printClassName() {
            System.out.println("Кошка");
        }
    }

    public static class Dog implements Printable {

        @Override
        public void printClassName() {
            System.out.println("Собака");
        }
    }

    public static class Bird implements Printable {

        @Override
        public void printClassName() {
            System.out.println("Птица");
        }
    }

    public static class Lamp implements Printable {

        @Override
        public void printClassName() {
            System.out.println("Лампа");
        }
    }

    private interface Printable {
        void printClassName();
    }
}