package intrestingMomentsLuckOfDoc.reflectionQuestion1;

public class QAAnswer2 {

    public static void main(String[] args) {
        printObjectType(new Cat());
        printObjectType(new Bird());
        printObjectType(new Lamp());

        printObjectType(new Cat());

        printObjectType(new Dog());
    }

    public static void printObjectType(Object o) {
        Class<?> classType = o.getClass();
        if (classType == Cat.class) {
            System.out.println("");
        } else if (classType == Bird.class) {
            System.out.println("Птица");
        } else if (classType == Dog.class) {
            System.out.println("Собака");
        }
    }

    public static class Cat {

    }

    public static class Dog {
    }

    public static class Bird {
    }

    public static class Lamp {
    }
}