package intrestingMomentsLuckOfDoc.reflectionQuestion1;

import java.lang.reflect.Field;

/**
 * Или «Кошка», или «Собака», или «Птица», или «Лампа»
 * Написать метод, который определяет, объект какого класса ему передали, и выводит на экран одну из надписей: Кошка, Собака, Птица, Лампа.
 */
public class qa_learning_1_complete
{
    public static void main(String[] args)
    {
        printObjectType(new Cat());
        printObjectType(new Bird());
        printObjectType(new Lamp());
        printObjectType(new Cat());
        printObjectType(new Dog());
    }

    /**
     * Method search class of object and gives answer what it is. This for QA about Reflection.
     * @param o
     * Gives some object to a method.
     */
    public static void printObjectType(Object o)
    {
      //Напишите тут ваше решение
        Class check = o.getClass();
        Field field;
        try
        {
            field = check.getField("subjectName");
            System.out.println(field.get(o));
        }
        catch (NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Some abstract inner class
     */
    protected static abstract class Subject {
        public static String subjectName;

        private Subject(String subjectName) {
            this.subjectName = subjectName;
        }
    }

    public static class Cat extends Subject
    {
        public Cat() {
            super("Кошка");
        }
    }

    public static class Dog extends Subject
    {
        public Dog() {
            super("Собака");
        }
    }

    public static class Bird extends Subject
    {
        public Bird() {
            super("Птица");
        }
    }

    public static class Lamp extends Subject
    {
        private Lamp()
        {
            super("Лампа");
        }
    }
}