package intrestingMomentsLuckOfDoc.formaterShow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by Nyarukou SAMA on 12.01.2016.
 * This class created to show possibilities of method join()
 */
public class JoinStringTest {
    public static void main(String[] args) {
        String str1 = "Hello";
        String str2 = "dear";
        String str3 = "QALJ!";
        ArrayList strForJoin = new ArrayList<CharSequence>(Arrays.asList(str1, str2, str3));
        String delimiter = " ";
        String stringFormat = "This prints result of join ('%s')%n";
        formatedMsgJoin(stringFormat, delimiter, strForJoin);
    }

    /**
     * With giving wrong object method calls about this with warnings showing what wrong objects are u give and how much ))
     * @param stringFormat
     * Use stringFormat means give the format of output
     * @param delimiter
     * The same as deLimiter in join
     * ArrayList of objects for joining
     * @param objForJoin
     * ArrayList of objects for joining
     */
    public static void formatedMsgJoin(String stringFormat, String delimiter, ArrayList objForJoin) {
        if (delimiter != null && stringFormat != null) {
            ArrayList<CharSequence> trueCharSequence = new ArrayList<>();
            ArrayList garbage = new ArrayList();
            int garbageCount = 0;
            int nullCount = 0;
            for (Object obj : objForJoin) {
                if (obj != null) {
                    if (obj.getClass() == String.class)
                        trueCharSequence.add((CharSequence) obj);
                    else {
                        garbage.add(obj);
                        garbageCount++;
                    }
                } else nullCount++;
            }
            System.out.print(String.format(Locale.ENGLISH, stringFormat, String.join(delimiter, trueCharSequence)));
            if (garbageCount > 0)
                System.out.printf("You have the following garbage objects %h in quantity %d%n", garbage, garbageCount);
            if (nullCount > 0)
                System.out.printf("You have the following null values number %d", nullCount);
        } else {
            System.out.println("Delimiter or StringFormat is not defined!");
        }
    }
}
