package intrestingMomentsLuckOfDoc.formaterShow;

import java.util.Locale;

/**
 * Created by Nyarukou SAMA on 12.01.2016.
 * printf() demo 2!
 */
public class StringFormaterQA2 {
    public static void main(String[] args) {
        System.out.printf("%.20g%n%.20f%n", Math.PI, Math.E);
        System.out.printf(Locale.CHINA, "%,010.5f", Math.PI);

    }
}
