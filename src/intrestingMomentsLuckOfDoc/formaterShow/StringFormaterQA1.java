package intrestingMomentsLuckOfDoc.formaterShow;

/**
 * Created by Nyarukou SAMA on 12.01.2016.
 * printf() demo 1!
 */

public class StringFormaterQA1 {
    public static void main(String[] args) {
        int[][] someArray = new int[10][10];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                someArray[i][j] = (i + 1) * (j + 1);
                System.out.printf("%4d", someArray[i][j]);
            }
            System.out.printf("%n");
        }
    }
}
