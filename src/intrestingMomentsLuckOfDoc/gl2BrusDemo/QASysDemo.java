package intrestingMomentsLuckOfDoc.gl2BrusDemo;

/**
 * Created by Nyarukou SAMA on 14.01.2016.
 * Some notes about...
 */
public class QASysDemo {
    /**
     * Show System class (different methods usage)
     * @param args
     * Standard parameter for method main
     */
    public static void main(String[] args) {
        System.getProperties().list(System.out);
        System.out.println(System.getProperty("user.name"));
        System.out.println(System.getProperty("java.library.path"));
    }
}
