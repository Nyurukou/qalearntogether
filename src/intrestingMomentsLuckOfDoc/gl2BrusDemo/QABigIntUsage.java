package intrestingMomentsLuckOfDoc.gl2BrusDemo;

import java.math.BigInteger;

//: object/Documentation1.
/**
 * Created by Nyarukou SAMA on 13.01.2016.
 * Some notes about.
 * <ol>
 *     <li> Testing 1.
 *     <li> Testing 2.
 *     <li> Testing 3.
 * </ol>
 */
public class QABigIntUsage {
    /**
     * BigInteger short test
     * @param args
     * it's main param of method main()
     */
    public static void main(String[] args) {
        BigInteger bigInteger1 = new BigInteger("7589275209380283482034021737193401239412734812309482103841723984");
        BigInteger bigInteger2 = new BigInteger("7465284759273824890528034850928346587698208576237528386276523827");
        System.out.println(bigInteger1.multiply(bigInteger2));
    }
}
